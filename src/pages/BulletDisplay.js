import { useParams } from "react-router-dom";
import SockJsClient from "react-stomp";
import { useState, useEffect } from "react";
import BulletScreen, { StyledBullet } from "rc-bullets";

const SOCKET_URL = "http://localhost:8080/bullet-chat-message"; // need to change production url

const BulletDisplay = () => {
  const { sessionId } = useParams();

  let onConnected = () => {
    //console.log("connected");
  };

  let onMessageReceived = (msg) => {
    screen.push(
      <StyledBullet
        head={null}
        msg={msg}
        backgroundColor={"#fff"}
        size="small"
      />
    );
  };

  let onDisconnected = () => {
    //console.log("Disconnected!");
  };

  // eslint-disable-next-line
  const [screen, setScreen] = useState(null);

  useEffect(() => {
    let s = new BulletScreen(".screen", { duration: 10 });
    setScreen(s);
  }, []);

  return (
    <div>
      <SockJsClient
        url={SOCKET_URL}
        topics={[`/bullet-chat-room/${sessionId}`]}
        onConnect={onConnected}
        onDisconnect={onDisconnected}
        onMessage={onMessageReceived}
        debug={false}
      />
      <div
        className="screen"
        style={{
          width: "100vw",
          height: "20vh",
          position: "absolute",
        }}
      ></div>
      <div
        style={{
          width: "100vw",
          height: "100vh",
          backgroundImage: `url(${"https://media.giphy.com/media/Cmr1OMJ2FN0B2/giphy.gif"})`,
          backgroundSize: "cover",
        }}
      ></div>
    </div>
  );
};

export default BulletDisplay;

import { createBrowserRouter } from "react-router-dom";
import BulletDisplay from "../pages/BulletDisplay";

const router = createBrowserRouter([
  {
    path: "/:sessionId",
    element: <BulletDisplay />,
  },
  {
    path: "*",
    element: <div>Please go to a bullet chat room</div>,
  },
]);

export default router;
